<?php

namespace ThinkCreative\SearchBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;


/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ThinkCreativeSearchExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // load config
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        // set parameters
        if (!$container->hasParameter("think_creative_search.solr")) {
            $container->setParameter("think_creative_search.solr", $config['solr']);    
        }
        if (!$container->hasParameter("think_creative_search.search_page_template")) {
            $container->setParameter("think_creative_search.search_page_template", $config['search_page_template']);
        }
        if (!$container->hasParameter("think_creative_search.search")) {
            $container->setParameter("think_creative_search.search", $config['search']);
        }
        if (!$container->hasParameter("think_creative_search.filter")) {
            $container->setParameter("think_creative_search.filter", $config['filter']);
        }

        // load services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

    }

    public function prepend(ContainerBuilder $container) {
        $doctrineConfig = [];
        $doctrineConfig['orm']['entity_managers']['user_data']['mappings'] = array(
            'ThinkCreativeSearchBundle' => array('mapping' => 1),
        );
        $container->prependExtensionConfig('doctrine', $doctrineConfig);

    }

}
