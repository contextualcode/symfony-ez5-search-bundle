<?php

namespace ThinkCreative\SearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('think_creative_search');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()

                // Solr Conf
                ->arrayNode("solr")
                    ->children()
                        ->scalarNode("host")
                            ->isRequired()
                        ->end()
                        ->integerNode("port")
                            ->defaultValue(8986)
                        ->end()
                        ->scalarNode("protocol")
                            ->defaultValue("http")
                        ->end()
                        ->scalarNode("path")
                            ->defaultValue("solr")
                        ->end()
                        ->scalarNode("core")
                            ->isRequired()
                        ->end()
                    ->end()
                ->end()

                // Search Template
                ->scalarNode("search_page_template")
                    ->defaultValue("ThinkCreativeSearchBundle:parts:search_base.html.twig")
                ->end()

                // Filter
                ->scalarNode("filter")
                    ->defaultValue("")
                ->end()

                // Searh Conf
                ->arrayNode("search")
                    ->prototype("array")
                        ->children()
                            ->scalarNode("name")
                            ->end()
                            ->scalarNode("query_handler")
                            ->end()
                            ->scalarNode("query_fields")
                            ->end()
                            ->scalarNode("query")
                            ->end()
                            ->integerNode("limit")
                                ->defaultValue(10)
                            ->end()
                            ->scalarNode("highlight_fields")
                            ->end()
                            ->booleanNode("highlighting")
                                ->defaultValue(true)
                            ->end()
                            ->scalarNode("fl")
                                ->defaultValue("*,score")
                            ->end()

                            // Sort
                            ->arrayNode("sort")
                                ->defaultValue(array(
                                    array(
                                        "name" => "Relevance",
                                        "value" => "score desc"
                                    )
                                ))
                                ->prototype("array")
                                    ->children()
                                        ->scalarNode("name")
                                            ->isRequired()
                                        ->end()
                                        ->scalarNode("value")
                                            ->isRequired()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()

                            // Facet Fields
                            ->arrayNode("facet_fields")
                                ->prototype("array")
                                    ->children()
                                        ->scalarNode("display_name")
                                        ->end()
                                        ->scalarNode("identifier")
                                        ->end()
                                        ->scalarNode("field")
                                            ->isRequired()
                                        ->end()
                                        ->scalarNode("prefix")
                                        ->end()
                                        ->scalarNode("sort")
                                            ->defaultValue("alpha")
                                        ->end()
                                        ->integerNode("limit")
                                            ->defaultValue(20)
                                        ->end()
                                        ->integerNode("offset")
                                            ->defaultValue(0)
                                        ->end()
                                        ->integerNode("mincount")
                                            ->defaultValue(0)
                                        ->end()
                                        ->booleanNode("missing")
                                            ->defaultValue(false)
                                        ->end()
                                        ->scalarNode("operator")
                                            ->defaultValue("AND")
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()

                            // Facet Ranges
                            ->arrayNode("facet_ranges")
                                ->prototype("array")
                                    ->children()
                                        ->scalarNode("display_name")
                                        ->end()
                                        ->scalarNode("identifier")
                                        ->end()
                                        ->arrayNode("fields")
                                            ->isRequired()
                                            ->prototype("scalar")->end()
                                        ->end()
                                        ->scalarNode("start")
                                            ->isRequired()
                                        ->end()
                                        ->scalarNode("end")
                                            ->isRequired()
                                        ->end()
                                        ->arrayNode("gaps")
                                            ->isRequired()
                                            ->prototype("array")
                                                ->children()
                                                    ->scalarNode("name")
                                                        ->isRequired()
                                                    ->end()
                                                    ->scalarNode("value")
                                                        ->isRequired()
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()

                            // Facet Queries
                            ->arrayNode("facet_queries")
                                ->prototype("array")
                                    ->children()
                                        ->scalarNode("display_name")
                                        ->end()
                                        ->scalarNode("identifier")
                                        ->end()
                                        ->arrayNode("queries")
                                            ->prototype("array")
                                                ->children()
                                                    ->scalarNode("display_name")
                                                    ->end()
                                                    ->scalarNode("identifier")
                                                    ->end()
                                                    ->scalarNode("query")
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                        ->scalarNode("operator")
                                            ->defaultValue("AND")
                                        ->end()                                        
                                    ->end()
                                ->end()
                            ->end()

                            ->scalarNode("facet_query_operator")
                                ->defaultValue("AND")
                            ->end()

                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
