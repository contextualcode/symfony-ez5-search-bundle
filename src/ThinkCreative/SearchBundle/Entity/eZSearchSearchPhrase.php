<?php

namespace ThinkCreative\SearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="ezsearch_search_phrase")
 * @ORM\HasLifecycleCallbacks()
 */
class eZSearchSearchPhrase
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $phrase;

    /**
     * @ORM\Column(type="integer")
     */
    private $phrase_count;

    /**
     * @ORM\Column(type="integer")
     */
    private $result_count;

    /**
     * Set phrase
     *
     * @param string $phrase
     *
     * @return eZSearchSearchPhrase
     */
    public function setPhrase($phrase) {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * Get phrase
     *
     * @return string
     */
    public function getPhrase() {
        return $this->phrase;
    }

    /**
     * Set phrase_count
     *
     * @param string $phrase_count
     *
     * @return eZSearchSearchPhrase
     */
    public function setPhraseCount($phrase_count) {
        $this->phrase_count = $phrase_count;
        return $this;
    }

    /**
     * Get phrase_count
     *
     * @return integer
     */
    public function getPhraseCount() {
        return $this->phrase_count;
    }

    /**
     * Set result_count
     *
     * @param string $result_count
     *
     * @return eZSearchSearchPhrase
     */
    public function setResultCount($result_count) {
        $this->result_count = $result_count;
        return $this;
    }

    /**
     * Get result_count
     *
     * @return integer
     */
    public function getResultCount() {
        return $this->result_count;
    }

}
