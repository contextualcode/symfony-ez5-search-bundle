<?php

namespace ThinkCreative\SearchBundle\Solr;
use ThinkCreative\SearchBundle\Solr\SolrFacetField;
use ThinkCreative\SearchBundle\Solr\SolrFacetRanges;
use ThinkCreative\SearchBundle\Solr\SolrFacetQuery;
use ThinkCreative\SearchBundle\Exception\SolrApiException;

class SolrQuery
{

    /**
     * URI to make queries too.
     * @var string $requestUri
     */
    protected $requestUri;

    /**
     * Array of params to build query with.
     * @var array $urlParams
     */
    protected $urlParams = array();

    /**
     * Array of ThinkCreative\SearchBundle\Classes\SolrFacetField.
     * @var array $facetFields
     */
    protected $facetFields = array();

    /**
     * Array of ThinkCreative\SearchBundle\Classes\SolrFacetRanges.
     * @var array $facetRanges
     */
    protected $facetRanges = array();

    /**
     * Array of ThinkCreative\SearchBundle\Classes\SolrFacetQueries.
     * @var array $facetQueries
     */
    protected $facetQueries = array();

    /**
     * Logical operation for stringing facet queries together
     * @var string
     */
    protected $facetQueryOperator = "AND";

    /**
     * Filter to convert field names to readable name.
     * @var object $filter
     */
    protected $filter = null;

    protected $logger = null;

    /**
     * Constructor.
     * @param string $host      Solr server host.
     * @param string $core      Solr core.
     * @param integer $port     Solr server port.
     * @param string $protocol  Solr server protocal (HTTP or HTTPS)
     * @param string $path      Solr server path.
     * @param object $filter    Filter object for field name and value conversions.
     */
    public function __construct($host, $core, $port = 8986, $protocol = "http", $path = "solr", $filter = null, $logger = null)
    {

        // format vars
        $path = trim($path, "/");

        // default protocal
        if (!trim($protocol)) {
            $protocol = "http";
        }

        // generate request uri
        $this->requestUri = "{$protocol}://{$host}:{$port}/{$path}/{$core}/select";

        // filter
        $this->filter = $filter;

        $this->logger = $logger;
    }

    /** 
     * Set operator to use for facet query search logic
     * @param string $operator
     */
    public function setFacetQueryOperator($operator = "AND")
    {
        $this->facetQueryOperator = $operator;
    }

    /**
     * Set filter service to use with queries.
     * @param object $filter_service
     */
    public function setFilter($filter_service)
    {
        $this->filter = $filter_service;
    }

    /**
     * Set/get search query to perform.
     * @param string value
     * @return SolrQuery|string
     */
    public function query($value = "")
    {
        return $this->param('q', $value);
    }

    /**
     * Set/get filter query
     * @param string value
     * @return string
     */
    public function fq($value = "")
    {
        return $this->param('fq', $value);
    }

    /**
     * Set/get query fields
     * @param string value
     * @return string
     */
    public function qf($value = "")
    {
        return $this->param('qf', $value);
    }

    /**
     * Set/get query handler
     * @param string value
     * @return string
     */
    public function qt($value = "")
    {
        return $this->param('qt', $value);
    }

    /**
     * Set/get search result limit.
     * @param integer value
     * @return SolrQuery|integer
     */
    public function limit($value = null)
    {
        return $this->param("rows", $value);
    }

    /**
     * Set/get search result offset.
     * @param integer value
     * @return SolrQuery|integer
     */
    public function offset($value = null)
    {
        return $this->param("start", $value);
    }

    /**
     * Set/get search result highlighting
     * @param boolean value
     * @return SolrQuery|boolean
     */
    public function highlighting($value = null)
    {
        return $this->param("hl", $value ? "true" : "false");
    }

    /**
     * Set/get search result highlighting fields
     * @param string value
     * @return SolrQuery|boolean
     */
    public function highlightingFields($value = null)
    {
        return $this->param("hl.fl", $value);
    }
    

    /**
     * Set/get sorting.
     * @param string value
     * @return SolrQuery|boolean
     */
    public function sort($value = null)
    {
        return $this->param("sort", $value);
    }

    /**
     * Get/set search parameter.
     * @param string $name
     * @param mixed $value
     * @return SolrQuery|mixed
     */
    public function param($name, $value = "")
    {

        // Set
        if ($value !== NULL) {

            // value must be int or string
            if (!in_array(gettype($value), array("string", "integer", "boolean", "double"))) {
                return $this;
            }

            if (array_key_exists($name, $this->urlParams)) {
                if (gettype($this->urlParams[$name]) != "array") {
                    $this->urlParams[$name] = array($this->urlParams[$name]);    
                }
                $this->urlParams[$name][] = $value;
            } else {
                $this->urlParams[$name] = $value;
            }
            return $this;
        }
        // Get
        if (!array_key_exists($name, $this->urlParams)) {
            return false;
        }
        return $this->urlParams[$name];
    }

    /**
     * Create a new SolrFacetField.
     * @return ThinkCreative\SearchBundle\Classes\SolrFacetField
     */
    public function facetField()
    {
        $this->param('facet', 'true');
        $newFacet = new SolrFacetField($this);
        $this->facetFields[] = $newFacet;
        return $newFacet;
    }

    /**
     * Create a new SolrFacetRange.
     * @return ThinkCreative\SearchBundle\Classes\SolrFacetRange
     */
    public function facetRange()
    {
        $this->param('facet', 'true');
        $newFacet = new SolrFacetRange($this);
        $this->facetRanges[] = $newFacet;
        return $newFacet;
    }

    /**
     * Create a new SolrFacetQuery.
     * @return ThinkCreative\SearchBundle\Classes\SolrFacetQuery
     */
    public function facetQuery()
    {
        $this->param('facet', 'true');
        $newFacet = new SolrFacetQuery($this);
        $this->facetQueries[] = $newFacet;
        return $newFacet;
    }

    /**
     * Build search query.
     * @param boolean $use_facets
     * @return array
     */
    public function build($ignore_facets = array())
    {

        // json output
        $this->param("wt", "json");

        // score
        if (!$this->param("fl")) {
            $this->param("fl", "*,score");
        } 

        // build query string
        $query = http_build_query($this->urlParams, "", "&", PHP_QUERY_RFC3986);
        $query = preg_replace("/%5B[\\d]%5D/", "", $query);

        // build query string for facet fields
        foreach ($this->facetFields as $facetField) {
            $query .= "&" . $facetField->build();
        }

        // build query string for facet range
        foreach ($this->facetRanges as $facetRange) {
            $query .= "&" . $facetRange->build();
        }

        // build query string for facet queries
        foreach ($this->facetQueries as $facetQuery) {
            $query .= "&" . $facetQuery->build();
        }

        // build facet query
        $fq = "";
        foreach ($this->facetFields as $facetField) {
            if ($facetField->fq() && !in_array($facetField->identifier(), $ignore_facets)) {
                $fq .= "&fq=";
                $fq .= $facetField->fq();
            }
        }
        foreach ($this->facetRanges as $facetRange) {
            if ($facetRange->fq() && !in_array($facetRange->identifier(), $ignore_facets)) {
                $fq .= "&fq=";
                $fq .= $facetRange->fq();
            }
        }

        $isFirst = true;
        foreach ($this->facetQueries as $facetQuery) {
            if ($facetQuery->fq() && !in_array($facetQuery->identifier(), $ignore_facets)) {
                switch ($this->facetQueryOperator) {
                    case "OR":
                    {
                        if ($isFirst) {
                            $fq .= "&fq=";
                            $isFirst = false;
                        }
                        $fq .= $facetQuery->fq();
                        $fq .= "%20OR%20";
                        break;
                    }
                    case "AND":
                    default:
                    {
                        $fq .= "&fq=";
                        $fq .= $facetQuery->fq();
                        break;
                    }
                }
            }
        }
        if (substr($fq, -8) == "%20OR%20") {
            $fq = substr($fq, 0, -8);
        }

        return $fq ? ($query . $fq) : $query;

    }

    /**
     * Submits current search parameters to Solr server.
     * Returns results, query string can be provided that
     * overrides all stored search parameters.
     * @param string $query
     * @return array
     */
    public function search($query = "")
    {

        // build query(s)
        if (!$query) {
            $query = $this->build();
        }

        // perform query        
        if (!$query) {
            return;
        }

        // perform http request to solr server
        if ($this->logger) {
            $this->logger->debug(
                "Search request: " .
                $this->requestUri . "?" . $query
            );
        }

        $context = stream_context_create(array('http'=>
            array(
                'timeout' => 10,
            )
        ));
        $resultStr = @file_get_contents($this->requestUri . "?" . $query, false, $context);

        // perform searches based on facets to produce the correct facet counts when OR operator is involved
        $facetResultStr = array();
        foreach ($this->facetFields as $facetField) {
            if ($facetField->operator() == "AND") {
                continue;
            }
            $facetResultStr[$facetField->identifier()] = @file_get_contents($this->requestUri . "?" . $this->build(array($facetField->identifier())));
        }
        foreach ($this->facetRanges as $facetRange) {
            if ($facetRange->operator() == "AND") {
                continue;
            }
            $facetResultStr[$facetRange->identifier()] = @file_get_contents($this->requestUri . "?" . $this->build(array($facetRange->identifier())));
        }
        foreach ($this->facetQueries as $facetQuery) {
            if ($facetQuery->operator() == "AND") {
                continue;
            }
            $facetResultStr[$facetQuery->identifier()] = @file_get_contents($this->requestUri . "?" . $this->build(array($facetQuery->identifier())));
        }

        // no results, error
        if ($resultStr === false) {
            if ($this->logger) {
                $this->logger->error(
                    "Could not connect to Solr server with given parameters."
                );
            }
            throw new SolrApiException("Could not connect to Solr server with given parameters.");
            return array();
        }

        // http response is not 200, error
        if (!strpos($http_response_header[0], "200")) { 
            if ($this->logger) {
                $this->logger->error(
                    "Received an error from the Solr server: " . $http_response_header[0]
                );
            }
            throw new SolrApiException("Recieved an error from the Solr server.");
            return array();
        }

        // json decode
        $results = json_decode($resultStr, true);

        $facetResults = array();
        foreach ($facetResultStr as $key => $resultStr) {
            $facetResults[$key] = json_decode($resultStr, true);
        }

        // no results
        if (!$results) {
            throw new SolrApiException("Did not receieve any search results.");
        }

        // facet formating
        $facetFields = array();
        $facetRanges = array();
        $facetQueries  = array();

        if ($results && array_key_exists("facet_counts", $results)) {
            // format facet fields
            if (array_key_exists("facet_fields", $results['facet_counts'])) {

                foreach ($this->facetFields as $facetField) {

                    $facetFieldList = $results['facet_counts']['facet_fields'];
                    if ($facetField->operator() == "OR" && isset($facetResults[$facetField->identifier()]['facet_counts']['facet_fields'])) {
                        $facetFieldList = $facetResults[$facetField->identifier()]['facet_counts']['facet_fields'];
                    }

                    foreach ($facetFieldList as $field => $values) {

                        // match fields
                        if ($facetField->field() != $field) {
                            continue;
                        }

                        for ($i = 0; $i < count($values); $i += 2) {
             
                            // get count
                            $count = $values[$i + 1];
                            if ($facetField->operator() == "OR" && isset($resultNoFacet)) {

                                foreach ($results['facet_counts']['facet_fields'] as $countField => $countValues) {
                                    
                                    // match fields
                                    if ($facetField->field() != $countField) {
                                        continue;
                                    }

                                    for ($j = 0; $j < count($countValues); $j += 2) {
                                        if ($countValues[$i] == $values[$i]) {
                                            $count = $countValues[$i + 1];
                                            break;
                                        }
                                    }
                                }                                
                            }

                            $displayName = $this->filter && method_exists($this->filter, $field) ? $this->filter->$field($values[$i]) : $values[$i];
                            if ($displayName) {
                                $facetFields[$facetField->displayName()][] = array(
                                    "identifier"   => $facetField->identifier(),
                                    "display" => $displayName,
                                    "count"   => $count,
                                    "query"   => array($facetField->identifier() => $values[$i]),
                                    "query_value"   => $values[$i]
                                );
                            }
                        }

                    }
                }
            }

            // format facet ranges
            if (array_key_exists("facet_queries", $results['facet_counts'])) {
                foreach ($this->facetRanges as $facetRange) {
                    $thisRange = array();
                    foreach ($facetRange->getQueries() as $gapName => $queryArray) {

                        // make query string
                        $queryStr = "";
                        foreach ($queryArray as $field => $string) {
                            if ($queryStr) {
                                $queryStr .= " OR ";
                            }
                            $queryStr .= "{$field}:[{$string}]";
                        }

                        // display name
                        $display = "";
                        foreach ($facetRange->gaps as $gap) {
                            if ($gap['value'] == $gapName) {
                                $display = $gap['name'];
                                break;
                            }
                        }

                        $thisRange[$gapName] = array(
                            "identifier"   => $facetRange->identifier(),
                            "display" => $display,
                            "gap"   => $gapName,
                            "count" => $results['facet_counts']['facet_queries'][$queryStr],
                            "query" => array($facetRange->identifier() => $gapName),
                            "query_value" => $gapName
                        );
                        
                    }
                    $facetRanges[$facetRange->displayName()] = $thisRange;
                }
            }

            // format facet queries
            if (array_key_exists("facet_queries", $results['facet_counts'])) {

                foreach ($this->facetQueries as $facetQuery) {

                    $facetQueryList = $results['facet_counts']['facet_queries'];
                    if ($facetQuery->operator() == "OR" && isset($facetResults[$facetQuery->identifier()]['facet_counts']['facet_queries'])) {
                        $facetQueryList = $facetResults[$facetQuery->identifier()]['facet_counts']['facet_queries'];
                    }

                    foreach ($facetQueryList as $field => $value) {

                        foreach ($facetQuery->getQueries() as $query) {
                            if ($field == $query['query']) {
                                $facetQueries[$facetQuery->displayName()][] = array(
                                    "identifier"   => $facetQuery->identifier(),
                                    "display" => $query['display_name'],
                                    "count"   => $value,
                                    "query"   => $query['query'],
                                    "query_value"   => $query['identifier']
                                );

                            }
                        }
                    }
                }
            }

        }

        return array(
            "solr" => &$results,
            "facets" => array(
                "fields" => &$facetFields,
                "ranges" => &$facetRanges,
                "queries" => &$facetQueries
            )
        );

    }

}
