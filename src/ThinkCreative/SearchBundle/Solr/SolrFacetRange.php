<?php

namespace ThinkCreative\SearchBundle\Solr;

class SolrFacetRange
{

    /**
     * Counter to counter number of instances.
     * @var integer $instanceCount
     */
    protected static $instanceCount = 0;

    /**
     * Parent query object.
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery $parent
     */
    protected $parent;

    /**
     * Array of fields to use in this range.
     * @var array $fields
     */
    protected $fields = array();

    /**
     * Range start value. Can be numeric or DateTime string.
     * @var string $start
     */
    protected $start;

    /**
     * Range end value. Can be numeric or DateTime string.
     * @var string $start
     */
    protected $end;

    /**
     * Facet query value
     * @var string
     */
    protected $value;

    /**
     * Array of string that determine how range will be
     * split up. Array key is the display name for this
     * range.
     * @var array $gaps
     */
    public $gaps = array();

    /**
     * Identifier that uniquely identifies this
     * object instances. SPL_OBJECT_HASH used
     * if identifier isn't provided.
     * @var string $identifier
     */
    public $identifier = "";

    /**
     * Name to display to the user.
     * @var string $displayName
     */
    public $displayName = "";

    /**
     * Construct.
     * @param ThinkCreative\SearchBundle\Solr\SolrQuery $parent
     */
    public function __construct(\ThinkCreative\SearchBundle\Solr\SolrQuery $parent)
    {
        $this->parent = $parent;
        $this->identifier = "range_" . (++self::$instanceCount);
        $this->displayName = $this->identifier;
    }

    /**
     * Get/set list of fields to use in this range.
     * @return SolrFacetRange|array
     */
    public function fields(array $value = array())
    {
        // set
        if ($value) {
            $this->fields = $value;
            return $this;
        }
        // get
        return $this->fields;
    }

    /**
     * Get/set start range.
     * @return SolrFacetRange|string
     */
    public function start($value = null)
    {
        // set
        if ($value) {
            $this->start = $value;
            return $this;
        }
        // get
        return $this->start;
    }

    /**
     * Get/set end range.
     * @return SolrFacetRange|string
     */
    public function end($value = null)
    {
        // set
        if ($value) {
            $this->end = $value;
            return $this;
        }
        // get
        return $this->end;
    }

    /**
     * Get/set gaps.
     * @return SolrFacetRange|array
     */
    public function gaps(array $value = array())
    {

        // set
        if ($value) {
            $this->gaps = $value;
            return $this;
        }
        // get
        return $this->gaps;
    }

    /**
     * Get/set identifier.
     * @return SolrFacetRange|string
     */
    public function identifier($value = "")
    {
        // set
        if ($value) {
            $this->identifier = preg_replace("/[^A-Za-z0-9_]/", '', $value);
            return $this;
        }
        // get
        return $this->identifier;
    }

    /**
     * Get/set display name.
     * @return SolrFacetRange|string
     */
    public function displayName($value = "") 
    {
        // set
        if ($value) {
            $this->displayName = $value;
            return $this;
        }
        // get
        return $this->displayName;
    }

    /**
     * Get array of facet queries to perform for
     * this range.
     * @return array
     */
    public function getQueries()
    {
        $queries = array();
        for ($i = 0; $i < count($this->gaps); $i++) {
            
            foreach ($this->fields as $field) {
        
                $query = "";

                // get query var
                if ($i < count($this->gaps) - 1) {
                    $query .= $this->end . $this->gaps[$i]['value'];
                } else {
                    $query .= $this->start;
                }
                $query .= " TO ";
                if ($i < count($this->gaps) - 1) {
                    $query .= $this->end;
                } else {
                    $query .= $this->end . $this->gaps[$i]['value'];
                }

                $queries[$this->gaps[$i]['value']][$field] = $query;

            }

        }

        return $queries;
    }

    /**
     * Get final URL string for this range.
     * @return string
     */
    public function build()
    {
        $queryOutput = "";
        foreach ($this->getQueries() as $gap => $queryArray) {
            $queryInline = "";
            foreach ($queryArray as $field => $queryStr) {
                if ($queryInline) {
                    $queryInline .= " OR ";
                }
                $queryInline .= "{$field}:[{$queryStr}]";
            }

            if ($queryOutput) {
                $queryOutput .= "&";
            }
            $queryOutput .= "facet.query=" . $queryInline;
        }

        $queryOutput = urlencode($queryOutput);
        $queryOutput = str_replace("%3D", "=", $queryOutput);
        $queryOutput = str_replace("%26", "&", $queryOutput);

        return $queryOutput;
    }

    /**
     * Set facet query value
     * @param string $value
     */
    public function value($value)
    {
        $this->value = $value;
    }

    /**
     * Returns filter query to perform
     * @param string $value
     * @return string
     */
    public function fq()
    {

        if (!$this->value) {
            return "";
        }

        $queries = $this->getQueries();

        $queryStr = "";
        foreach ($queries[$this->value] as $field => $query) {
            if (!$queryStr) {
                $queryStr .= "(";
            } else {
                $queryStr .= " OR ";
            }
            $queryStr .= "{$field}:[{$query}]";
        }
        if ($queryStr) {
            $queryStr .= ")";
        }

        return urlencode($queryStr);
    }

    /**
     * Returns $parent
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery
     */
    public function done()
    {
        return $this->parent;
    }
}