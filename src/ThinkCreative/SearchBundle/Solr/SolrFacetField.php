<?php

namespace ThinkCreative\SearchBundle\Solr;

class SolrFacetField
{

    /**
     * Counter to counter number of instances.
     * @var integer $instanceCount
     */
    protected static $instanceCount = 0;

    /**
     * Parent query object.
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery $parent
     */
    protected $parent;

    /**
     * Array of parameters to append to search
     * query string.
     * @var array $urlParams
     */
    protected $urlParams = array();

    /** 
     * Array of values to apply to facet query
     * @var array
     */
    protected $values = array();

    /**
     * Operator to use when stringing facets together
     * @param string
     */
    protected $operator = "AND";

    /**
     * Identifier that uniquely identifies this
     * object instances. SPL_OBJECT_HASH used
     * if identifier isn't provided.
     * @var string $identifier
     */
    public $identifier = "";

    /**
     * Name to display to the user.
     * @var string $displayName
     */
    public $displayName = "";

    /**
     * Construct.
     * @param ThinkCreative\SearchBundle\Solr\SolrQuery $parent
     */
    public function __construct(\ThinkCreative\SearchBundle\Solr\SolrQuery $parent)
    {
        $this->parent = $parent;
        $this->identifier = "field_" . (++self::$instanceCount);
        $this->displayName = $this->identifier;
    }

    /**
     * Get/set field name for this facet.
     * @param string $value
     * @return SolrFacetField|string
     */
    public function field($value = null)
    {
        return $this->param('facet.field', $value);
    }

    /**
     * @param string $value
     * @return SolrFacetField|string
     */
    public function prefix($value = null)
    {
        return $this->param('facet.prefix', $value);
    }

    /**
     * @param string $value
     * @return SolrFacetField|string
     */
    public function sort($value = null)
    {
        return $this->param('facet.sort', $value);
    }

    /**
     * @param integer $value
     * @return SolrFacetField|integer
     */
    public function limit($value = null)
    {
        return $this->param('facet.limit', $value);
    }

    /**
     * @param integer $value
     * @return SolrFacetField|integer
     */
    public function offset($value = null)
    {
        return $this->param('facet.offset', $value);
    }

    /**
     * @param integer $value
     * @return SolrFacetField|integer
     */
    public function mincount($value = null)
    {
        return $this->param('facet.mincount', $value);
    }

    /**
     * @param boolean $value
     * @return SolrFacetField|boolean
     */
    public function missing($value = null)
    {
        return $this->param('facet.missing', $value ? "true" : "");
    }

    /**
     * Get/set unique identifier for this object.
     * @param string $value
     * @return SolrFacetField|string
     */
    public function identifier($value = "")
    {
        // set identifier
        if ($value) {
            $this->identifier = preg_replace("/[^A-Za-z0-9_]/", '', $value);
            return $this;
        }

        // get identifier
        return $this->identifier;
    }

    /**
     * Get/set display name for this object.
     * @param string $value
     * @return SolrFacetField|string
     */
    public function displayName($value = "")
    {

        // set
        if ($value) {
            $this->displayName = $value;
            return $this;
        }
        // get
        return $this->displayName;
    }

    /**
     * Get/set a URL parameter for this facet.
     * @param string $name
     * @param string $value
     * @return mixed  Returns $this if setting, mixed if getting.
     */
    public function param($name, $value = null)
    {
        // Set value
        if ($value !== NULL) {
            $this->urlParams[$name] = $value;
            return $this;
        }

        // Get value
        if (!array_key_exists($name, $this->urlParams)) {
            return false;
        }
        return $this->urlParams[$name];
    }

    /**
     * Set facet query values
     * @param array $values
     */
    public function values(array $values)
    {

        $this->values = $values;
    }

    /**
     * Get/Set operator to use to string facets together
     * @param string $value
     */
    public function operator($value = "")
    {
        if ($value) {
            $this->operator = trim(strtoupper($value));
        } else {
            return $this->operator;
        }
    }

    /**
     * Build URL query string.
     * @return string
     */
    public function build()
    {

        // facet.field must be set
        if (!array_key_exists("facet.field", $this->urlParams)) {
            return false;
        }

        // return url params
        return http_build_query($this->urlParams);
    }

    /**
     * Returns filter query to perform
     * @param string $value
     * @return string
     */
    public function fq()
    {

        if (!$this->values) {
            return "";
        }

        $query = "";
        foreach ($this->values as $value) {

            if ($query) {
                $query .= "+" . $this->operator . "+";
            }
            $query .= $this->field() . ':"' . urlencode($value) . '"';
        }

        return trim($query);
    }

    /**
     * Returns $parent
     * @var ThinkCreative\SearchBundle\Classes\SolrQuery
     */
    public function done()
    {
        return $this->parent;
    }

}