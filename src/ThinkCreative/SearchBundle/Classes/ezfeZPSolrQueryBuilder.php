<?php
namespace ThinkCreative\SearchBundle\Classes;
use ezfeZPSolrQueryBuilder as ezfeZPSolrQueryBuilderParent;

class ezfeZPSolrQueryBuilder extends ezfeZPSolrQueryBuilderParent {
    public function publicPolicyLimitationFilterQuery( $limitation = null, $ignoreVisibility = null ) {
        return $this->policyLimitationFilterQuery($limitation, $ignoreVisibility);
    }
    
    public function publicGetClassAttributes( $classIDArray = false, $classAttributeIDArray = false, $fieldTypeExcludeList = null ) {
        return $this->getClassAttributes( $classIDArray, $classAttributeIDArray, $fieldTypeExcludeList );
    }
}