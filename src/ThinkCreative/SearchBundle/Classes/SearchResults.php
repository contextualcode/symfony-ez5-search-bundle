<?php

namespace ThinkCreative\SearchBundle\Classes;
use Pagerfanta\Adapter\FixedAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrapView;

/**
 * Class containing results of a SOLR search
 */
class SearchResults
{

    /**
     * Array containing results of search.
     * @var array
     */
    protected $resultsArray;

    /**
     * Pagerfanta pager
     * @var Pagerfanta
     */
    protected $pager;

    /**
     * Constructor.
     * @param array $results_array
     */
    public function __construct(array $results_array)
    {
        // results array
        $this->resultsArray = $results_array;

        // remove *:* wildcard character from search query
        if (trim($this->resultsArray["params"]["query"]) == "*:*") {
            $this->resultsArray["params"]["query"] = "";
        }

        // build pager object
        if (isset($this->resultsArray['solr']['response']['docs'])) {

            // build adapter
            $adapter = new FixedAdapter($this->getNumberFound(), $this->resultsArray['solr']['response']['docs']);

            // get pager
            $this->pager = new Pagerfanta($adapter);
            $this->pager->setMaxPerPage(
                isset($this->resultsArray['solr']['responseHeader']['params']['rows']) ? $this->resultsArray['solr']['responseHeader']['params']['rows'] : 10
            );
            $this->pager->setCurrentPage(
                floor( (isset($this->resultsArray['solr']['responseHeader']['params']['start']) ? $this->resultsArray['solr']['responseHeader']['params']['start'] : 0) / $this->pager->getMaxPerPage() ) + 1
            );        
        }
    }

    /**
     * Return query string.
     */
    public function getQueryString()
    {
        return stripslashes(isset($this->resultsArray['solr']['responseHeader']['params']['q']) ? strip_tags(trim(str_replace("*:*", "", $this->resultsArray['solr']['responseHeader']['params']['q']))) : "");
    }

    /**
     * Return number of results.
     */
    public function getNumberFound()
    {
        return isset($this->resultsArray['solr']['response']['numFound']) ? $this->resultsArray['solr']['response']['numFound'] : 0;
    }

    /**
     * Return search results array
     */
    public function getResults()
    {
        return isset($this->resultsArray['solr']['response']['docs']) ? $this->resultsArray['solr']['response']['docs'] : array();
    }

    /**
     * Get a Pagerfanta pagination object
     * @return Pagerfanta
     */
    public function getPager()
    {
        return $this->pager;
    }

    /**
     * Render pagination HTML for Twitter Bootstrap
     * @param array $params
     * @param string $baseUrl
     * @return string
     */
    public function renderControlsForTwitterBootstrap(array $options = array(), $base_url = "", $urlParams = array())
    {

        // get pager
        $pager = $this->getPager();

        // get params
        if (!$urlParams) {
            $urlParams = $this->getSearchParams();
        }

        // router generator
        $routeGenerator = function($page) use ($pager, $urlParams, $base_url) {
            $urlParams['offset'] = ($page * $pager->getMaxPerPage()) - $pager->getMaxPerPage();
            return str_replace('"','&quot;', $base_url . "?" . urldecode(http_build_query($urlParams)));
        };

        // create view
        $view = new TwitterBootstrapView();
        $options = array_merge($options, array(
            'proximity' => 3,
            'prev_message' => "&laquo;",
            'next_message' => "&raquo;"
        ));
        $output = $view->render($pager, $routeGenerator, $options);

        // slight hack to get the 'pagination' class on the UL element
        $output = str_replace("<ul", "<ul class='pagination pagination-sm'", $output);

        return $output;
    }

    /**
     * Returns true if there are facets
     * @param boolean
     */
    public function hasFacets()
    {
        if (!isset($this->resultsArray['facets'])) {
            return false;
        }
        foreach ($this->resultsArray['facets'] as $facet) {
            if (count($facet) > 0)  {
                return true;
            }
        }
        return false;
    }

    /**
     * Return array containing all types of facets as key
     * @return array
     */
    public function getAllFacets()
    {
        if (!$this->hasFacets()) {
            return array();
        }

        $arr = array();
        foreach ($this->resultsArray['facets'] as $facets) {
            $arr = array_merge($arr, $facets);
        }
        return $arr;
    }

    /**
     * Return facet fields of given type.
     * @param string $type
     * @return array
     */
    public function getFacets($type)
    {
        return isset($this->resultsArray['facets'][$type]) ? $this->resultsArray['facets'][$type] : array();
    }

    /**
     * Get facet fields
     * @return array
     */
    public function getFacetFields() {
        return $this->getFacets("fields");
    }

    /**
     * Get facet ranges
     * @return array
     */
    public function getFacetRanges() {
        return $this->getFacets("ranges");
    }

    /**
     * Get facet queries
     * @return array
     */
    public function getFacetQueries() {
        return $this->getFacets("queries");
    }

    /**
     * Get facet counts
     * @return array
     */
    public function getFacetCounts() {
        if (!isset($this->resultsArray['solr']['facet_counts'])) {
            return array();
        }

        return $this->resultsArray['solr']['facet_counts'];
    }

    /**
     * Get highest score in search results
     * @return integer
     */
    public function getMaxScore()
    {
        return isset($this->resultsArray['solr']['response']['maxScore']) ? $this->resultsArray['solr']['response']['maxScore'] : 0;
    }

    /**
     * Get array of params used to perform search
     */
    public function getSearchParams()
    {
        return isset($this->resultsArray['params']) ? $this->resultsArray['params'] : array();
    }

    /**
     * Get highlighting
     */
    public function getHighlighting()
    {
        return isset($this->resultsArray['solr']['highlighting']) ? $this->resultsArray['solr']['highlighting'] : array();   
    }

    /**
     * Get highlighting texts
     */
    public function getHighlightingTexts()
    {
        $highlighting      = $this->getHighlighting();
        $highlightingTexts = array();
        foreach( $highlighting as $id => $items ) {
            if( count( $items ) === 0 ) {
                continue;
            }
            
            $text = '';
            foreach( $items as $attr => $highlights ) {
                $text .= implode( ' ', $highlights );
            }
            $highlightingTexts[ $id ] = $text;
        }

        return $highlightingTexts;   
    }

    /**
     * Generate URL query string from search params and
     * optional additional parameters
     * @param array $additionalParams
     * @return string
     */
    public function getUrlParamString($additionalParams = array())
    {
        $searchParams = array_merge(
            $this->getSearchParams(),
            $additionalParams
        );
        return urldecode(http_build_query($searchParams));
    }

}
