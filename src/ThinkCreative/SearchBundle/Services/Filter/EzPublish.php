<?php

namespace ThinkCreative\SearchBundle\Services\Filter;
use Symfony\Component\HttpFoundation\Request;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;

class EzPublish
{

    /**
     * EzPublish Content Type Service
     * @var eZ\Publish\API\Repository\ContentTypeService $contentTypeServer
     */
    protected $contentTypeService;

    /**
     * EzPublish Content Service
     * @var eZ\Publish\API\Repository\ContentService $contentService
     */
    protected $contentService;

    /**
     * EzPublish Location Service
     * @var eZ\Publish\API\Repository\LocationService $locationService
     */
    protected $locationService;

    public function __construct(\eZ\Publish\Core\SignalSlot\Repository $ezPublishApiRepository)
    {
        $this->contentTypeService = $ezPublishApiRepository->getContentTypeService();
        $this->contentService = $ezPublishApiRepository->getContentService();
        $this->locationService = $ezPublishApiRepository->getLocationService();
    }

    /**
     * Takes value of field 'meta_class_identifier_ms' and
     * returns ezPublish class identifier name.
     * @param string $value
     * @return string
     */
    public function meta_class_identifier_ms($value)
    {

        try {
            $contentType = $this->contentTypeService->loadContentTypeByIdentifier($value);
            if (!$contentType) {
                return ucwords($value);
            }
        } catch (NotFoundException $e) {
            return ucwords($value);
        }

        // @TODO get current language
        $languages = array_keys($contentType->names);
        return ucwords($contentType->names[ $languages[0] ]);
    }

    /**
     * Get the name of a location from location id
     * @param integer $location_id
     * @return string
     */
    private function getLocationName($location_id)
    {
        // @TODO bypass permissions?
        try {
            $location = $this->locationService->loadLocation($location_id);
            if ($location) {
                return $location->contentInfo->name;
            }
        } catch (UnauthorizedException $e) {
            
        } catch (NotFoundException $e) {
            
        }

        return "N/A";
    }

    public function meta_main_path_element_2_si($value)
    {
        return $this->getLocationName($value);
    }

    public function meta_main_parent_node_id_si($value)
    {
        return $this->getLocationName($value);
    }

}