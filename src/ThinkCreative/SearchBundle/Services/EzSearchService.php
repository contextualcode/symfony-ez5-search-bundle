<?php

namespace ThinkCreative\SearchBundle\Services;
use eZ\Publish\Core\Repository\ContentService;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use eZ\Publish\Core\MVC\Legacy\Kernel as LegacyKernel;
use ThinkCreative\SearchBundle\Services\SearchService;
use ThinkCreative\SearchBundle\Classes\SearchResults;
use ThinkCreative\SearchBundle\Services\Filter\EzPublish as EzPublishSearchFilter;
use eZSolr;
use ezpKernelHandler;
use ThinkCreative\SearchBundle\Classes\ezfeZPSolrQueryBuilder;


/**
 * Perform ez publish search
 */
class EzSearchService
{

    /**
     * Think Creative SOLR Search Service
     * @var SearchService
     */
    protected $searchService;

    /**
     * eZ Publish Content Service
     * @var ContentService
     */
    protected $ezContentService;
    
    /**
     * Solr query to process eZ Publish policies
     * @var string
     */
    protected $policyLimitationQuery;

    /**
     * Construtor.
     * @param array $solr_config
     * @param Repository $ez_publish_api_repository
     */
    public function __construct(SearchService $search_service, ContentService $ez_content_service, \Closure $ez_legacy_kernel, EzPublishSearchFilter $ez_search_filter)
    {
        $this->searchService = $search_service;
        $this->ezContentService = $ez_content_service;
        $this->searchService->setFilter($ez_search_filter);

        // build policy limitation query
        $this->policyLimitationQuery = $ez_legacy_kernel()->runCallback(
            function()
            {
                $ezSolr = new eZSolr();
                $qBuilder = new ezfeZPSolrQueryBuilder($ezSolr);
                return $qBuilder->publicPolicyLimitationFilterQuery();
            }
        );
    }

    /**
     * @see SearchService::getSortOptions
     */
    public function getSortOptions($search_type)
    {
        return $this->searchService->getSortOptions($search_type);
    }

    /**
     * Perform SOLR search with eZ Publish policy limitation query.
     * @param string $search_type
     * @param string $query
     * @param integer $limit
     * @param integer $offset
     * @param integer $sort
     * @param array $facet_values
     * @param array $solr_params  Extra parameters for SOLR query
     * @return SearchResults
     */
    public function search($search_type = "default", $query = "", $limit = null, $offset = 0, $sort = "", array $facet_values = array(), array $solr_params = array())
    {

        return $this->searchService->search(
            $search_type,
            $query,
            $limit,
            $offset,
            $sort,
            $facet_values,
            array_merge(
                array("fq" => $this->policyLimitationQuery),
                $solr_params
            )
        );
    }

    /**
     * Retrieve eZ Publish content from search results
     * @param SearchResults $search_results
     * @return \eZ\Publish\API\Repository\Values\Content\Content[]
     */
    public function getContentFromSearchResults(SearchResults $search_results)
    {
        $content = array();
        foreach ($search_results->getResults() as $result) {
            try {
                $content[] = $this->ezContentService->loadContent($result['meta_id_si']);
            } catch (NotFoundException $e) {
                $content[] = null;
            } catch (UnauthorizedException $e) {
                $content[] = null;
            }
        }
        return $content;
    }

    
    
}