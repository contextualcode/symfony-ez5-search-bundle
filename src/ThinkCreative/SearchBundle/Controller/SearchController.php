<?php

namespace ThinkCreative\SearchBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Publish\API\Repository\Values\Content\Content;
use Symfony\Component\HttpFoundation\Response;
use ThinkCreative\SearchBundle\Solr\SolrQuery;
use ThinkCreative\SearchBundle\Exception\SearchTypeNotAvailableException;
use ThinkCreative\SearchBundle\Event\ThinkcreativeSearchEvent;

class SearchController extends Controller
{

    /**
     * Main search action
     * @param string $query
     * @param string $search_type
     */
    public function indexAction($query = "", $search_type = "default")
    {

        // get request
        $request = $this->get("request");
        
        // get config params
        $searchPageTemplate = $this->container->getParameter("think_creative_search.search_page_template");
        $searchConfig = $this->container->getParameter("think_creative_search.search");
        $filter = $this->container->getParameter("think_creative_search.filter");

        // get params
        $query = $request->query->get("query") ?: "*:*";
        $qf = $request->query->get("qf");
        $limit = $request->query->get("limit") ?: null;
        $offset = $request->query->get("offset") ?: 0;
        $sort = $request->query->get("sort") ?: "";

        // load search service
        $searchService = $this->get("thinkcreative.search.search");

        // update search config to include qf
        if ($qf) {
            foreach ($searchConfig as $key => $searchConfigItem) {
                if (!isset($searchConfigItem['name']) || $searchConfigItem['name'] != trim($search_type)) {
                    continue;
                }
                if (!is_array($qf)) {
                    $qf = array($qf);
                }
                $searchConfig[$key]['query'] = (isset($searchConfig[$key]['query']) ? $searchConfig[$key]['query'] . " AND " : "") . "(" . implode(") AND (", $qf) . ")";
                $searchService->setSearchConfig($searchConfig);
                break;
            }
        }

        // set filter
        $searchService->setFilter($this->get($filter));

        // perform search
        $results = $searchService->search(
            $search_type,
            $query,
            $limit,
            $offset,
            $sort,
            $request->query->all()
        );

        $data = array($query, $results->getNumberFound());
        $event = new ThinkcreativeSearchEvent($data);
        $event_dispatcher = $this->get('event_dispatcher');
        $event_dispatcher->dispatch('thinkcreative.ezsearch.search.phrase.logger', $event);

        // Response
        $response = new Response();
        $response->setPublic();

        // render response
        return $this->render(
            $searchPageTemplate,
            array(
                "searchResults" => $results
            ),
            $response
        );

    }
}