<?php

namespace ThinkCreative\SearchBundle\EventListener;

use ThinkCreative\SearchBundle\Event\ThinkcreativeSearchEvent;
use Doctrine\ORM\EntityManager;
use ThinkCreative\SearchBundle\Entity\eZSearchSearchPhrase;
use ReflectionClass;

class eZSearchSearchPhraseLogger
{

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    public function onThinkcreativeSearch(ThinkcreativeSearchEvent $event) {

        $data = $event->getData();
        $phrase = $data[0];
        $count = $data[1];
        
        $filters = array(
            'phrase' => $phrase
        );

        $item = $this->em->getRepository(eZSearchSearchPhrase::class)->findOneBy($filters);

        if ($item === null) {
            $item = new eZSearchSearchPhrase();
            $item->setPhrase($phrase);
        }

        $item->setPhraseCount($item->getPhraseCount() + 1);
        $item->setResultCount($item->getResultCount() + $count);

        $this->em->persist($item);
        $this->em->flush();
    }
}
